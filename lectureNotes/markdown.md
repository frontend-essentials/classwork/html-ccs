# Header1
## Header2

> Fancy note

Just a text  
New line created via two spaces

[link](example.org)  

**bold text**  
***italic text***  
~~crossed text~~


#### Bullet list
* Item 1
* Item 2

#### Indexed list
1. Item 1
1. Item 2
150. Focus-pocus

```javascript
    console.log("Hello world");
let masha = {
hair: 'curly',
sex: 'girl',
sayHello: () => console.log('Hello!')
}
    // This is code snippet
```